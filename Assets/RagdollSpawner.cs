﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RagdollSpawner : MonoBehaviour
{
    [SerializeField] private float min;
    [SerializeField] private float max;
    [SerializeField] private GameObject ragdollPrefabs;

    // Update is called once per frame
    void Update()
    {
        SpawnObject();
    }

    private void SpawnObject()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            var randomPosX = Random.Range(min, max);
            Vector3 spawnPos = new Vector3(randomPosX, 5f,0);
            Instantiate(ragdollPrefabs, spawnPos, transform.rotation);
        }
    }
}
